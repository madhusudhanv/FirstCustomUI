var express = require('express');
var router = express.Router();
var misc = require('../src/misc');
var hmac = require('../src/hmac');
var http = require('request');
router.get('/', function(req, res) {
	res.render("index");
});

router.get('/login', function(req, res) {
	res.render("login");
});

router.get('/signup', function(req, res) {
	res.render("signup");
});

router.get('/welcome', function(req, res) {
	console.log(req.session);
	/*if(req.session.mobile) {
		res.render("welcome",{mobile:req.session.mobile});
	}
	else {
		res.redirect('/');
	}*/
	req.session.mobile = "919566183207";
	res.render("welcome",{mobile:req.session.mobile});
});

router.get('/logout', function(req, res) {
	req.session.destroy(function(err) {
		res.redirect('/');
	});
});

router.post('/login', misc.route(task(req, res) {
	catch (e) {
		console.log(e.stack);
	}
	var params = req.body;
	http.post({
		url: 'http://localhost:4000/login',
		headers: {
			'content-type': 'application/json'
		},
		body: JSON.stringify({
			'source': params.source,
			'mobile': params.mobile
		})
	}, function(err, response, body) {
		if (response.statusCode == 500) {
			res.status(500).send("failure");
		} else {
			console.log("before",req.session);
			req.session.mobile = params.mobile;
			console.log(req.session);
			res.send("Success");
		}
	});

}));



module.exports = router;