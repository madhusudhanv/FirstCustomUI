var express = require('express');
var path = require('path');
var logger = require('morgan');
var bodyParser = require('body-parser');
var jade = require('jade');
var cookieParser = require('cookie-parser');
var favicon = require('serve-favicon');
var flash = require('connect-flash');
var sweet = require('sweet.js');
var session = require('express-session');
sweet.loadMacro('cspjs');

var index = require('./routes/index');

var app = express();

// view engine setup
app.set('view engine', 'jade');
app.set('views', 'views');
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(__dirname + '/bower_components'));
app.use(flash());
app.use(favicon(path.join(__dirname, '/public/fingerprint.ico')));
app.use(session({secret: 'c!rcl#123'}));

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

// Routes
app.use('/', index);

// error handler
app.use(function(err, req, res, next) {
	err.status = err.status || 500;
	res.status(err.status);
	res.send({
		status: 'error',
		code: err.status,
		error: err.message || err.toString()
	});
});


module.exports = app;