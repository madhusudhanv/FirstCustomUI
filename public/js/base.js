(function() {
	$("#signup-form").submit(function(event) {
		event.preventDefault();
		swal({
			title: "Message Sent ! ",
			type: "info",
			text: "We have sent a sms to your mobile number.Please use your finger print to register.",
		},function () {
			$.ajax({
			type: "POST",
			url: "http://localhost:4000/register",
			data: {
				user_id: $('#user_id').val(),
				source: $('#source').val(),
				mobile: $('#mobile_num').val()
			},
			success: function(data, textStatus, jqXHR) {
				console.log(textStatus);
				swal({
					title: "Registration success",
					type: "info"
				});
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log(textStatus);
				swal({
					title: "Registration Failure",
					type: "error"
				});
			}
		});
		});
	});
	$("#login-form").submit(function(event) {
		event.preventDefault();
		var periodic_timer = "";
		var timer = 60 * 3,
			minutes, seconds;
		periodic_timer = setInterval(function() {
			minutes = parseInt(timer / 60, 10);
			seconds = parseInt(timer % 60, 10);

			minutes = minutes < 10 ? "0" + minutes : minutes;
			seconds = seconds < 10 ? "0" + seconds : seconds;
			$('#timer-widget').text(minutes + ":" + seconds);
			if (--timer < 0) {
				timer = duration;
			}
		}, 1000);
		$('#timer-modal').modal({
			keyboard: false,
			backdrop: "static"
		});
		$('#timer-modal').on('shown.bs.modal',function (e) {
			$.ajax({
			type: "POST",
			url: "http://localhost:5000/login",
			data: {
				source: $('#source').val(),
				mobile: $('#mobile_num').val()
			},
			success: function(data, textStatus, jqXHR) {
				$('#timer-modal').modal('hide');
				clearInterval(periodic_timer);
				console.log(textStatus);
				swal({
					title: "Login success",
					type: "info"
				}, function() {
					window.location = "/welcome";
				});
			},
			error: function(jqXHR, textStatus, errorThrown) {
				$('#timer-modal').modal('hide');
				clearInterval(periodic_timer);
				console.log(textStatus);
				swal({
					title: "Login Failure",
					type: "error"
				});
			}
		});
		});
	

	});
})();

function startTimer(duration, display) {
	var timer = duration,
		minutes, seconds;
	var periodic_timer = setInterval(function() {
		minutes = parseInt(timer / 60, 10);
		seconds = parseInt(timer % 60, 10);

		minutes = minutes < 10 ? "0" + minutes : minutes;
		seconds = seconds < 10 ? "0" + seconds : seconds;

		display.text(minutes + ":" + seconds);

		if (--timer < 0) {
			timer = duration;
		}
	}, 1000);
	return periodic_timer;
}