/**
 * hmac.js - Provides unguessable public id generation for aspects of the system.
 * Copyright (c) 2014, imaginea.com
 */


var crypto = require('crypto');


// The "targets" identify different purposes for which the generated id may be used.
// Generally, use 'server' for server-server id creation. 
var targets = {
    '%server':  'pEt9Spfj2yrNuJibmwkpo8O8XeZaU4N1iF',
    '%db': 'yDLBjiMG6efdF2JKdkw5WfRDJ2XLqTNh1',
    '%client': '8WzbvInRtNOgnDj9aaDoOwHRjbulxJHG2naQE'
};

// Converts first 8 bytes of the buffer into base 32 string.
var b32str = '0123456789abcdefghijklmnopqrstuv';

function bit2mask(bit) {
    return (1 << bit) - 1;
}

// Converts given buffer to an N-character base32 string that's easy to display
// and small enough for use in URLs without scaring people.
function base32(buffer, N) {
    N = N || 12;
    var str = '', byteStartIx, bitStartIx, byteEndIx, bitEndIx, b1, b2, i, byteIx = 0, bitIx = 0, nibble;
    while (str.length < N) {
        if (bitIx + 5 <= 8) {
            nibble = (buffer[byteIx] >> (3 - bitIx)) & bit2mask(5);
        } else {
            nibble = ((buffer[byteIx] & bit2mask(8 - bitIx)) << (bitIx - 3)) | ((buffer[byteIx+1] >> (11 - bitIx)) & bit2mask(bitIx - 3));
        }
        str += b32str[nibble];
        bitIx += 5;
        if (bitIx >= 8) {
            byteIx++;
            bitIx -= 8;
        }
    }
    return str;
}

function hmacFull(target, str, extra, N) {
    var secret = targets['%'+target];
    if (secret) {
        var hmac = crypto.createHmac('sha512', secret);
        hmac.update(str);
        if (extra) {
            hmac.update('_' + extra);
        }
        return base32(hmac.digest(), N);
    } else {
        throw new Error('Invalid hmac target - ' + target);
    }
}

// Call as hmac('server', "some string to be turned into an id, could be JSON, for example.");
// If a third argument is given, it is assumed to be a Date object and yyyymmdd will be used
// to additionally stamp the hmac.
//
// hmac will give you a 12 character result (60 bits).
// hmac.long will give you a 32 character result (160 bits).
// hmac.very_long will give you a 102 character result (510 bits).
function hmac(target, str, extra) {
    return hmacFull(target, str, extra, 12);
}

// 01234567
hmac.short = function (target, str, extra) {
    return hmacFull(target, str, extra, 8);
};

// 01234567890123456789012345678901
hmac.long = function (target, str, extra) {
    return hmacFull(target, str, extra, 32);
};

// 012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901
hmac.very_long = function (target, str, extra) {
    return hmacFull(target, str, extra, 102);
};

hmac.salt = function (n) {
    try {
        return crypto.randomBytes(16).toString('hex');
    } catch (e) {
        return '4';
    }
};

module.exports = hmac;

